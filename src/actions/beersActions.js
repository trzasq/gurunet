import axios from "axios";
const baseUrl = "https://api.punkapi.com/v2";
const perPage = 20;

export const GET_BEERS_DONE = 'GET_BEERS_DONE';
export const GET_SPECIFIC_BEER_DONE = 'GET_SPECIFIC_BEER_DONE';
export const SHOW_LOADER = 'SHOW_LOADER';
export const HIDE_LOADER = 'HIDE_LOADER';
export const SET_INITIAL_DIMENSIONS = 'SET_INITIAL_DIMENSIONS';
export const EMPTY_BEER = 'EMPTY_BEER';
export const ALL_DATA_FETCHED = 'ALL_DATA_FETCHED';


export const SIMILAR_ABV_DONE = 'SIMILAR_ABV_DONE';
export const SIMILAR_EBC_DONE = 'SIMILAR_EBC_DONE';
export const SIMILAR_IBU_DONE = 'SIMILAR_IBU_DONE';

export const similarAbvDone = (beer) => ({ type: SIMILAR_ABV_DONE, beer });
export const similarEbcDone = (beer) => ({ type: SIMILAR_EBC_DONE, beer });
export const similarIbuDone = (beer) => ({ type: SIMILAR_IBU_DONE, beer });
export const emptyBeer = () => ({ type: EMPTY_BEER });
export const showLoader = () => ({ type: SHOW_LOADER });
export const hideLoader = () => ({ type: HIDE_LOADER });
export const getBeersDone = (payload) => ({ type: GET_BEERS_DONE, payload });
export const getSpecificBeerDone = (payload) => ({ type: GET_SPECIFIC_BEER_DONE, payload });
export const setInitialDimensions = (width, height, x, y) => ({ type: SET_INITIAL_DIMENSIONS, width, height, x, y });
export const allDataFetched = () => ({ type: ALL_DATA_FETCHED });

const getBeerWithSlightyHihgerAbv = (abv) => {
  return dispatch => {
    return axios.get(`${baseUrl}/beers`, {
      params: {
        abv_gt: parseInt(abv, 10)
      }
    }).then(response => {
      let reduced = response.data.reduce((prev, curr) => {
        return Math.abs(curr.abv - abv) < Math.abs(prev.abv - abv) ? curr : prev;
      });
      dispatch(similarAbvDone(reduced));

    })
  }
}
const getBeerWithSlightyHihgerEbc = (ebc) => {
  return dispatch => {
    return axios.get(`${baseUrl}/beers`, {
      params: {
        ebc_gt: parseInt(ebc, 10)
      }
    }).then(response => {
      let reduced = response.data.reduce((prev, curr) => {
        return Math.abs(curr.ebc - ebc) < Math.abs(prev.ebc - ebc) ? curr : prev;
      });
      dispatch(similarEbcDone(reduced));

    })
  }
}
const getBeerWithSlightyHihgerIbu = (ibu) => {
  return dispatch => {
    return axios.get(`${baseUrl}/beers`, {
      params: {
        ibu_gt: parseInt(ibu, 10)
      }
    }).then(response => {
      let reduced = response.data.reduce((prev, curr) => {
        return Math.abs(curr.ibu - ibu) < Math.abs(prev.ibu - ibu) ? curr : prev;
      });
      dispatch(similarIbuDone(reduced));
    })
  }
}


export const letsGetBeersTogether = (page) => {
  return dispatch => {
    dispatch(showLoader());
    axios
      .get(`${baseUrl}/beers/`, {
        params: {
          page,
          per_page: perPage
        }
      })
      .then(response => {
        dispatch(hideLoader());
        if (response.status === 200) {
          dispatch(getBeersDone(response.data));
          if (response.data.length < 20) {
            dispatch(allDataFetched())
          }

          // dispatch(getCardsToDeleteDone(response.data));
        }
      })
      .catch(error => {
        dispatch(hideLoader());
        if (error.response) {
        } else {
        }
      });
  };
};

export const getTheChosenOneBeer = (id) => {
  return dispatch => {
    // dispatch(showLoader());
    axios
      .get(`${baseUrl}/beers/${id}`, {

      })
      .then(response => {
        // dispatch(hideLoader());
        if (response.status === 200) {
          dispatch(getSpecificBeerDone(response.data[0]));
          dispatch(getBeerWithSlightyHihgerAbv(response.data[0].abv))
          dispatch(getBeerWithSlightyHihgerIbu(response.data[0].ibu))
          dispatch(getBeerWithSlightyHihgerEbc(response.data[0].ebc))
        }
      })
      .catch(error => {
        if (error.response) {
        } else {
        }
      });
  };
};

