import React, { Component } from "react";
import styled, { keyframes } from "styled-components";

const speedSwitch = (speed) => {
  if (speed === "fast") return 600
  if (speed === "slow") return 900
  return 750
}

const spinning = keyframes`
from {transform: rotate(0deg)}
to {transform: rotate(360deg)}
`;
const StyledVector = styled.svg`
transition-property: transform;
animation-name:${spinning} ;
animation-iteration-count: infinite;
animation-timing-function: linear;
animation-duration: ${speedSwitch(props => (props.speed))}ms;
`;

class Spinner extends Component {

  render() {
    const { thickness, color, gap, size } = this.props;

    return (<StyledVector
      height={size}
      width={size}
      role="img"
      aria-labelledby="title desc"
      viewBox="0 0 32 32"
    >
      <title id="title">Circle loading spinner</title>
      <desc id="desc">Image of a partial circle indicating "loading."</desc>

      <circle
        role="presentation"
        cx={16}
        cy={16}
        r={14 - (thickness / 2)}
        stroke={color}
        fill="none"
        strokeWidth={thickness}
        strokeDasharray={Math.PI * 2 * (11 - gap)}
        strokeLinecap="round"
      />
    </StyledVector>)


  }
}
Spinner.defaultProps = {
  color: "rgba(0,0,0,0.4)",
  gap: 4,
  thickness: 4,
  size: "2em",
  speed: 'fast'
}

export default Spinner;
