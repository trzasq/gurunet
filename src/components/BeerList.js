import React, { Component } from "react";
import { letsGetBeersTogether, setInitialDimensions } from "../actions/beersActions";
import { connect } from "react-redux";
// import PropTypes from "prop-types";
import Card from "./Card";
import Box from "./Box";
import Spinner from "./Spinner";

class BeerList extends Component {
  constructor() {
    super();
    this._onGimmeThatBeer = this._onGimmeThatBeer.bind(this);
  }
  componentDidMount() {
  }

  _onGimmeThatBeer(event) {
    const { setInitialDimensions } = this.props;
    const width = event.currentTarget.offsetWidth;
    const height = event.currentTarget.offsetHeight;
    const x = event.currentTarget.offsetLeft;
    let y = event.currentTarget.offsetTop;

    const element = document.getElementById('scroll-container')

    const scrolled = element.scrollTop
    y -= scrolled;

    let percentWidth = Math.round((width / element.clientWidth) * 100);
    let percentHeight = Math.round((height / element.clientHeight) * 100);
    let percentY = Math.round((y / element.clientHeight) * 100);
    let percentX = Math.round((x / element.clientWidth) * 100);


    const { beerId } = event.currentTarget.dataset;
    setInitialDimensions(percentWidth, percentHeight, percentX, percentY);
    this.props.history.push(`beer/${beerId}`);
  }


  render() {
    const { beers, loading } = this.props;

    return (
      <Box background='transparent' style={{ flexWrap: "wrap", justifyContent: "center" }}>
        {beers.length > 0 &&
          beers.map(beer => (
            <Card

              key={beer.id} data-beer-id={beer.id} onClick={this._onGimmeThatBeer}>
              <img alt='' style={{ maxHeight: 50 + '%', flex: 1 }} src={beer.image_url} />
              <Box justify='center' align='center' direction='column'>
                <h2 style={{ color: "#ffd602", margin: 0 }}>{beer.name} </h2>
                <h3 style={{ flex: 1, margin: 0 }}>{beer.tagline} </h3>
              </Box>
            </Card>
          ))}
        {loading && <Box direction='column' justify='flex-end' align='center'><Spinner /></Box>
        }




      </Box>

    );
  }
}

const mapStateToProps = state => {
  return {
    beers: state.beersReducer.beers,
    loading: state.beersReducer.loading

  };
};
const mapDispatchToProps = dispatch => ({
  letsGetBeersTogether: (page) => dispatch(letsGetBeersTogether(page)),
  setInitialDimensions: (width, height, x, y) => dispatch(setInitialDimensions(width, height, x, y))
});

export default BeerList = connect(mapStateToProps, mapDispatchToProps)(BeerList);
