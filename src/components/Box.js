import React, { Component } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const StyledBox = styled.div`
  display: flex;
  flex-direction: ${props => props.direction};
  justify-content: ${props => props.justify};
  background-color: ${props => props.background};
  align-items: ${props => props.align};
  margin: ${props => (props.margin === "small" ? "0" : "8px 16px")};
  padding: ${props => (props.padding === "small" ? "4px 8px" : "8px 16px")};
  text-align: center;
`;

class Box extends Component {
  render() {
    // const { pad, direction, align, justify } = this.props;
    return (
      <StyledBox {...this.props}>
        {this.props.children}
      </StyledBox>
    );
  }
}

Box.propTypes = {
  direction: PropTypes.string,
  align: PropTypes.string,
  justify: PropTypes.string,
  backgroundColor: PropTypes.string,
  margin: PropTypes.string,
  padding: PropTypes.string,

};

Box.defaultProps = {
  pad: { horizontal: "none", vertical: "none", between: "small" },
  direction: "row",
  align: "start",
  justify: "start",
  background: "none",
  margin: "small",
  padding: "small",
};

export default Box;
