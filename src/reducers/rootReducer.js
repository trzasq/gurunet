import {combineReducers} from 'redux';
import beersReducer from './beers';

const rootReducer = combineReducers({
  beersReducer
});

export default rootReducer;