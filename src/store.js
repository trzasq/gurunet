import {createStore, applyMiddleware} from 'redux';

// import {saveState, loadState} from './utils/localStorage';
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';
// import {routerMiddleware} from 'react-router-redux';
// import throttle from 'lodash/throttle';

//wczytywanie stanu z localhost
// const persistedState = loadState();

const reduxStore = createStore(rootReducer,  applyMiddleware(thunk));

// reduxStore.subscribe(throttle(() => {
//   saveState(reduxStore.getState());
// }, 2000))

export default reduxStore;