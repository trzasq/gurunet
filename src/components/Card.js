import React, { Component } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import Box from "./Box";

const StyledCard = styled(Box) `
  flex-basis: 20%;
  flex-shrink: 0;
  background-color: white;
  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, .14), 0 3px 1px -2px rgba(0, 0, 0, .2), 0 1px 5px 0 rgba(0, 0, 0, .12);
  justify-content: space-around;
  height: 400px;
  min-width: 200px;
  margin: 1.5%;
  transition: all 200ms ease-in;
  -webkit-font-smoothing: subpixel-antialiased;
  -webkit-font-smoothing: antialiased;
  cursor: pointer;
  
  &:hover {
    z-index:3;
    transition: all 200ms ease-in;
    transform: scale(1.1);
    -webkit-font-smoothing: subpixel-antialiased;
    -webkit-font-smoothing: antialiased;
    box-shadow: 0 4px 5px 0 rgba(0, 0, 0, .14), 0 1px 10px 0 rgba(0, 0, 0, .12), 0 2px 4px -1px rgba(0, 0, 0, .2);
  }
  @media (max-width: 700px) {
    &:hover {
      z-index:3;
      transition: all 200ms ease-in;
      transform: scale(1);
      -webkit-font-smoothing: subpixel-antialiased;
      -webkit-font-smoothing: antialiased;
      box-shadow: 0 4px 5px 0 rgba(0, 0, 0, .14), 0 1px 10px 0 rgba(0, 0, 0, .12), 0 2px 4px -1px rgba(0, 0, 0, .2);
    }
    }
`;

class Card extends Component {
  render() {
    // const { pad, direction, align, justify } = this.props;
    return (
      <StyledCard
        {...this.props}
        justify="center"
        align="center"
        direction="column"
      >
        {this.props.children}
      </StyledCard>
    );
  }
}

Card.propTypes = {
  fixed: PropTypes.bool
};

Card.defaultProps = {};

export default Card;
