import React, { Component } from "react";
import styled from "styled-components";

const StyledLayer = styled.div`
  z-index: 10;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  overflow: auto;
  background-color: rgba(0,0,0,0.2);
  position: fixed;
  top: 0px;
  left: 0px;
  right: 0px;
  bottom: 0px;
`;

class Layer extends Component {
  render() {
    return (
      <StyledLayer onClick={this.props.onClose} {...this.props} direction="row">
        {this.props.children}
      </StyledLayer>
    );
  }
}


export default Layer;
