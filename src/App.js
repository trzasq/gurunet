import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import BeerDetails from "./components/BeerDetails";
import BeerList from "./components/BeerList";
import styled, { ThemeProvider } from "styled-components";
import { connect } from "react-redux";
import { letsGetBeersTogether, showLoader } from "./actions/beersActions";
import { debounce } from 'lodash';
import { theme } from './theme';


const StyledApp = styled.div`
  background-color:${props => props.theme.colors.kindaGrey};;
  box-sizing: border-box;
  flex-direction: ${props => props.direction};
  justify-content: ${props => props.justify};
  display: flex;
  padding: 100px;
  height: 100vh;
  flex: 1;
  overflow: auto;
`;
const StyledHeading = styled.h2`
text-transform: uppercase;
margin:0 0 5% 10%`;

class App extends Component {
  constructor() {
    super();
    this.state = {
      layer: false,
      page: 1,
    };
    this._onToggleLayer = this._onToggleLayer.bind(this);
    this._onScroll = this._onScroll.bind(this);
    this._getMoreBeers = debounce(this._getMoreBeers.bind(this), 700);
  }
  _onToggleLayer() {
    this.props.history.push('/')
  }

  componentDidMount() {
    const { letsGetBeersTogether } = this.props;
    const { page } = this.state;
    this.setState((state) => ({ page: state.page + 1 }),
      letsGetBeersTogether(page)

    )
  }

  _getMoreBeers() {
    const { letsGetBeersTogether } = this.props;
    const { page } = this.state;

    this.setState((state) => ({ page: state.page + 1 }),
      letsGetBeersTogether(page)
    )
  }

  _onScroll(e) {
    const { showLoader, allDataFetched } = this.props;
    const { scrollHeight, offsetHeight, scrollTop } = e.target;


    if (scrollTop + offsetHeight + 20 >= scrollHeight && !allDataFetched) {
      showLoader();
      return this._getMoreBeers();
    }

  }
  render() {
    return (
      <ThemeProvider theme={theme}>
        <Router>
          <StyledApp id='scroll-container' onScroll={this._onScroll} direction="column" justify='space-around'>
            <StyledHeading>
              <span style={{ color: theme.colors.kindaYellow }}>beer</span>
              <span style={{ color: "#666666" }}>guru</span>
            </StyledHeading>
            <Route path="/" component={BeerList} />
            <Route path="/beer/:id" component={BeerDetails} />
          </StyledApp>
        </Router>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = state => {
  return {
    beers: state.beersReducer.beers,
    loading: state.beersReducer.loading,
    allDataFetched: state.beersReducer.allDataFetched
  };
};

const mapDispatchToProps = dispatch => ({
  letsGetBeersTogether: (page) => dispatch(letsGetBeersTogether(page)),
  showLoader: () => dispatch(showLoader())
});

export default App = connect(mapStateToProps, mapDispatchToProps)(App);
