# Netguru Task

This is a sample app for recruitment process for Netguru

App is based on CRA from facebook, To start execute the following commands:

  1. Install NPM modules

    ```
    $ npm install (or yarn install)
    ```

  2. Start the front-end dev server:

    ```
    $ npm start
    ```

### Release Notes

*  I tryied to match all specification but due to time limitations. The app lacks tests and errors handlers
*  Styles and props validation might be done better in real project
*  I will also use some selectors .
*  In advantage of the opportunity I played a bit with react-motion and styled-components I did not use too much

👋 Big up Netguru
  