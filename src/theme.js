export const theme = {
 sizes: {
  h2: '4vmin',
  h3: '3vmin',
  p: '2vmin'
 },
 colors: {
   kindaYellow: '#ffd602',
   kindaGrey: '#f5f5f5'
 }

}