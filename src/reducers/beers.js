import { GET_BEERS_DONE, SHOW_LOADER, HIDE_LOADER, GET_SPECIFIC_BEER_DONE, EMPTY_BEER, SET_INITIAL_DIMENSIONS, SIMILAR_ABV_DONE, SIMILAR_EBC_DONE, SIMILAR_IBU_DONE, ALL_DATA_FETCHED } from "../actions/beersActions";

const beersReducer = (
  state = {
    beers: [],
    beer: {},
    similarAbv: {},
    similarEbc: {},
    similarIbu: {},
    loading: false,
    allDataFetched: false,
    initialCardDimensions: {
      width: 0,
      height: 0,
      x: 0,
      y: 0
    }
  },
  action
) => {
  switch (action.type) {
    case GET_BEERS_DONE:
      return {
        ...state,
        beers: [...state.beers, ...action.payload]
      };
    case GET_SPECIFIC_BEER_DONE:
      return {
        ...state,
        beer: action.payload
      };
    case SHOW_LOADER:
      return {
        ...state,
        loading: true
      };
    case HIDE_LOADER:
      return {
        ...state,
        loading: false
      };
    case EMPTY_BEER:
      return {
        ...state,
        beer: {},
        similarAbv: {},
        similarEbc: {},
        similarIbu: {}
      };
    case SIMILAR_ABV_DONE:
      return {
        ...state,
        similarAbv: action.beer
      };
    case SIMILAR_EBC_DONE:
      return {
        ...state,
        similarEbc: action.beer
      };
    case SIMILAR_IBU_DONE:
      return {
        ...state,
        similarIbu: action.beer
      };
    case ALL_DATA_FETCHED:
      return {
        ...state,
        allDataFetched: true
      };
    case SET_INITIAL_DIMENSIONS:
      return {
        ...state,
        initialCardDimensions: { ...state.initialCardDimensions, height: action.height, width: action.width, x: action.x, y: action.y }
      };
    default:
      return state;
  }
};

export default beersReducer;
