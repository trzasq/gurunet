import React, { Component } from "react";
import styled, { withTheme } from "styled-components";
import { Motion, spring } from 'react-motion';
import { getTheChosenOneBeer, emptyBeer } from "../actions/beersActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Box from "./Box";
import Layer from "./Layer";

const Details = styled(Box) `
direction: column;
flex: 1 1 auto;
overflow-y: auto;
overflow-x: hidden;
position: absolute;
left: ${(props) => props.x}%;
top: ${(props) => props.y}%;
height: ${(props) => props.height}%;
width: ${(props) => props.width}%;
@media (max-width: 700px) {
  position: relative;
  width: 100vw;
  height: 100vh;
  top:0;
  left:0;
}

`;

const StyledH3 = styled.h3`
font-size:${props => props.size};
margin: 0;
@media (max-width: 700px) {
  font-size: 3vmin;

  }
`;

const StyledLi = styled.li`
font-size: 2vmin;
text-align: left;
@media (max-width: 700px) {
font-size: 3vmin;
}
`

const BorderedBox = styled(Box) `
position: relative;
&:after{
  content : "";
  position: absolute;
  bottom: 0;
  height  : 1px;
  width   : 100px;
  border-bottom:5px solid #ffd602;
}
`;

const StyledParagraph = styled.p`
font-size:${props => props.size};
text-align: left;
margin: 0;
@media (max-width: 700px) {
  font-size: 3vmin;
  }
`;

const StyledButton = styled.button`
position: absolute;
right: 0;
font-weight: 500;
background: none;
border: 0;
color: inherit;
cursor: pointer;
font: inherit;

`;

class BeerDetails extends Component {
  constructor() {
    super()
    this._onLayerClick = this._onLayerClick.bind(this);
  }
  componentDidMount() {
    const { id } = this.props.match.params
    const { getTheChosenOneBeer } = this.props;
    getTheChosenOneBeer(id);

  }

  _onLayerClick(e) {
    const { emptyBeer } = this.props;
    this.props.history.push('/');
    emptyBeer();

  }


  render() {
    const { beer, theme, similarAbv, similarEbc, similarIbu } = this.props;
    const { x, y, width, height } = this.props.initialCardDimensions;

    return (
      <Layer onClick={this._onLayerClick}>
        <Motion defaultStyle={{
          x,
          y,
          height,
          width

        }}
          style={{
            x: spring(30),
            y: spring(10),
            height: spring(80),
            width: spring(40)

          }}
        >
          {(style) => (
            <Details x={style.x} y={style.y} width={style.width} height={style.height} onClick={(e) => { e.stopPropagation() }} direction='column' background='#fff'>
              <Box style={{ flexBasis: 70 + '%' }} direction='row'>
                <img alt='beer_image' style={{ maxHeight: '100%', maxWidth: '100%' }} src={beer.image_url} />
                <Box style={{ flexShrink: 1 }} direction='column'>
                  <StyledButton onClick={this._onLayerClick}>x</StyledButton>
                  <BorderedBox direction='column' >
                    <StyledH3 size={theme.sizes.h3}>{beer.name}</StyledH3>
                    <styledH3 size={theme.sizes.h3}>{beer.tagline}</styledH3>
                  </BorderedBox>
                  <Box direction='row' justify='space-between'><Box><b>IBU</b> : {beer.ibu}</Box><Box><b>ABV</b>: {beer.abv}</Box><Box><b>EBC</b>: {beer.ebc}</Box></Box>
                  <Box><StyledParagraph size={theme.sizes.p}>{beer.description}</StyledParagraph></Box>
                  <Box direction='column'>
                    <StyledH3 size={theme.sizes.h3}>Best with:</StyledH3>
                    {beer.food_pairing && beer.food_pairing.length && <ul style={{ margin: 0, padding: 0, listStyleType: 'none' }}>
                      {beer.food_pairing.map(item => { return <StyledLi key={item} style={{ textStyledLign: 'left' }}>{`- ${item}`}</StyledLi> })}
                    </ul>}


                  </Box>

                </Box>
              </Box>
              <Box direction='column' style={{ flexBasis: 30 + '%', 'flexGrow': 1 }}>
                <StyledH3 size={theme.sizes.h3}>You might also like</StyledH3>
                <Box justify='space=between' direction='row'>
                  <Box align='center' direction='column'><img alt='proposal_image1' style={{ maxHeight: '50px', maxWidth: '100%' }} src={similarAbv.image_url} />{similarAbv.name}</Box>
                  <Box align='center' direction='column'><img alt='proposal_image2' style={{ maxHeight: '50px', maxWidth: '100%' }} src={similarEbc.image_url} />{similarEbc.name}</Box>
                  <Box align='center' direction='column'><img alt='proposal_image3' style={{ maxHeight: '50px', maxWidth: '100%' }} src={similarIbu.image_url} />{similarIbu.name}</Box>
                </Box>
              </Box>
            </Details>
          )
          }
        </Motion>
      </Layer>
    );
  }
}

BeerDetails.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  x: PropTypes.number,
  y: PropTypes.number,

};

BeerDetails.defaultProps = {};

const mapStateToProps = state => {
  return {
    beer: state.beersReducer.beer,
    similarAbv: state.beersReducer.similarAbv,
    similarEbc: state.beersReducer.similarEbc,
    similarIbu: state.beersReducer.similarIbu,
    loading: state.beersReducer.loading,
    initialCardDimensions: state.beersReducer.initialCardDimensions

  };
};

const mapDispatchToProps = dispatch => ({
  getTheChosenOneBeer: (id) => dispatch(getTheChosenOneBeer(id)),
  emptyBeer: () => dispatch(emptyBeer())
});

export default BeerDetails = withTheme(connect(mapStateToProps, mapDispatchToProps)(BeerDetails));
